import React, { useState } from 'react';
import {
  Container,
  Paper,
  Box,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
  Collapse,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// import { lightGreen } from '@material-ui/core/colors';
import {
  VerifiedUserOutlined,
  SettingsInputComponentOutlined,
  LocalGasStationRounded,
  SignalCellular0Bar,
} from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.grey[300],
  },
  sidebar: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '20vw',
    height: '100%',
    borderRadius: 0,
  },
  nestedItem: {
    paddingLeft: theme.spacing(8),
  },
}));

const ListComponent = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  return (
    <Container maxWidth='lg'>
      <Paper component={Box} className={classes.sidebar} boxShadow={4}>
        <List disablePadding>
          <ListItem button>
            <ListItemIcon>
              <VerifiedUserOutlined />
            </ListItemIcon>
            <ListItemText
              primary={
                <Typography variant='h5' color='primary'>
                  Getting started
                </Typography>
              }
              secondary='installation'
            />
          </ListItem>
          <Divider />
          <ListItem button>
            <ListItemIcon>
              <LocalGasStationRounded />
            </ListItemIcon>
            <ListItemText primary='components' />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <SettingsInputComponentOutlined />
            </ListItemIcon>
            <ListItemText primary='components api' />
          </ListItem>
          <ListItem button onClick={() => setOpen(!open)}>
            <ListItemIcon>
              <SignalCellular0Bar />
            </ListItemIcon>
            <ListItemText primary='styles' />
          </ListItem>
          <Collapse in={open}>
            <List disablePadding>
              <ListItem button className={classes.nestedItem}>
                <ListItemText>Nested Item 1</ListItemText>
              </ListItem>
              <ListItem button className={classes.nestedItem}>
                <ListItemText>Nested Item 2</ListItemText>
              </ListItem>
              <ListItem button className={classes.nestedItem}>
                <ListItemText>Nested Item 3</ListItemText>
              </ListItem>
            </List>
          </Collapse>
        </List>
      </Paper>
    </Container>
  );
};

export default ListComponent;
