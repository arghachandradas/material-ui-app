import React from 'react';
import Box from '@material-ui/core/Box';

const BoxComponent = () => {
  return (
    <Box width={500} p={3} boxShadow={4} component={'section'}>
      <h1>Get Started</h1>
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Mollitia
        perferendis error voluptates nam laborum eum quos impedit commodi
        eveniet doloremque officia earum dolore quibusdam placeat, tempora quia
        qui, praesentium accusamus?
      </p>
    </Box>
  );
};

export default BoxComponent;
