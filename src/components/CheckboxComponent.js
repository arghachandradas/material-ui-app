import React, { useState } from 'react';
import {
  Container,
  Paper,
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Favorite } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    backgroundColor: theme.palette.grey[300],
  },
}));

const CheckboxComponent = () => {
  const classes = useStyles();
  const [remember, setRemember] = useState(false);

  const handleChange = (e) => {
    setRemember(e.target.checked);
  };

  return (
    <Container className={classes.root}>
      <Paper component={Box} width='30%' mx='auto' p={4}>
        <Box component='form'>
          <FormControl>
            <FormControlLabel
              label='remember me'
              control={
                <Checkbox
                  icon={<Favorite />}
                  checkedIcon={<Favorite />}
                  onChange={handleChange}
                  checked={remember}
                  color='primary'
                />
              }
            />
          </FormControl>
        </Box>
      </Paper>
    </Container>
  );
};

export default CheckboxComponent;
