import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
}));

const Contact = () => {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.toolbar} />
      <p>Contact</p>
    </div>
  );
};

export default Contact;
