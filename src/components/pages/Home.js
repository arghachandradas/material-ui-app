import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
}));

const Home = () => {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.toolbar} />
      <p>Home</p>
    </div>
  );
};

export default Home;
