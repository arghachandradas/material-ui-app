import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
}));

const About = () => {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.toolbar} />
      <p>About</p>
    </div>
  );
};

export default About;
