import React, { useState } from 'react';
import {
  Container,
  Paper,
  Box,
  Switch,
  FormControlLabel,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { lightGreen } from '@material-ui/core/colors';
// import { Favorite } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    backgroundColor: theme.palette.grey[300],
  },
  light: {
    backgroundColor: lightGreen,
  },
}));

const ToggleSwitchComponent = () => {
  const classes = useStyles();
  const [autoplay, setAutoplay] = useState(false);

  const handleChange = (e) => {
    setAutoplay(e.target.checked);
  };

  return (
    <Container className={autoplay ? classes.root : classes.light}>
      <Paper component={Box} width='30%' mx='auto' p={4}>
        <Box component='form'>
          <FormControlLabel
            label='Change modes'
            control={<Switch onChange={handleChange} checked={autoplay} />}
          />
        </Box>
      </Paper>
    </Container>
  );
};

export default ToggleSwitchComponent;
