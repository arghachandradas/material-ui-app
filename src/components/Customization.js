import React from 'react';
import { Container, Paper, Box, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    backgroundColor: theme.palette.grey[200],
    paddingTop: theme.spacing(5),
  },
  mr: {
    marginRight: theme.spacing(2),
  },
}));

const Customization = () => {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      \
      <Paper component={Box} width='30%' mx='auto' p={3}>
        <Typography variant='h5' color='primary'>
          NodeJs latest features
        </Typography>
        <Typography gutterBottom>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aut tempore
          ad impedit obcaecati iusto! Harum voluptatum quaerat asperiores
          incidunt earum cumque nihil, quis a consectetur reiciendis? Harum ipsa
          quaerat veniam.
        </Typography>
        <Button variant='contained' color='primary' className={classes.mr}>
          Read More
        </Button>
        <Button variant='contained' color='secondary'>
          Share Post
        </Button>
      </Paper>
    </Container>
  );
};

export default Customization;
