import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  orange: {
    color: 'orange',
  },
}));

const TypographyComponent = () => {
  const classes = useStyles();

  return (
    <div>
      <Typography variant='h1' color='secondary'>
        Heading 1
      </Typography>
      <Typography variant='h2' display='inline'>
        Heading 2 (Inline)
      </Typography>
      <Typography variant='h3' display='inline'>
        Heading 3 (Inline)
      </Typography>
      <Typography variant='h4' className={classes.orange}>
        Heading 4
      </Typography>
      <Typography variant='h5' gutterBottom>
        Heading 5
      </Typography>
      <Typography variant='h6' gutterBottom>
        Heading 6
      </Typography>
      <Typography variant='body1'>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum.
      </Typography>
      <Typography variant='body2'>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum.
      </Typography>
      <Typography variant='subtitle1'>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum.
      </Typography>
      <Typography variant='subtitle2'>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum.
      </Typography>
      <Typography variant='subtitle2' align='justify'>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
        tempora culpa reprehenderit illo ullam odit ipsum sequi, repellendus
        nobis. Obcaecati est, dolorum quam ut distinctio error nihil quia natus
        eum.
      </Typography>
      <Typography variant='h3' align='center' color='primary'>
        Align Center
      </Typography>
    </div>
  );
};

export default TypographyComponent;
