import React from 'react';
import { Container, Button, Paper, Typography, Box } from '@material-ui/core';
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';

const ButtonComponent = () => {
  return (
    <Container maxWidth='md'>
      <Paper component={Box} p={5}>
        <Typography variant='h3'>Read Docs</Typography>
        <Typography variant='subtitle1'>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur
          molestias iure necessitatibus suscipit possimus ex consectetur odit ut
          eaque fuga, laudantium corrupti facere sit rerum cumque ipsa
          voluptatum officiis laboriosam.
        </Typography>
        <Box mt={3}>
          <Button
            color='secondary'
            variant='outlined'
            fullWidth
            size='large'
            startIcon={<AccessAlarmIcon />}
          >
            Read More
          </Button>
        </Box>
      </Paper>
    </Container>
  );
};

export default ButtonComponent;
