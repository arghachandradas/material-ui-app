import React from 'react';
import { Container, Typography, Button, Paper, Box } from '@material-ui/core';

const PaperComponent = () => {
  return (
    <Container maxWidth='sm'>
      {/* paper component have access to all the properties of box */}
      <Paper component={Box} p={5} square elevation={4}>
        <Typography variant='h3'>Read Docs</Typography>
        <Typography variate='subtitle1'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias
          adipisci voluptate maxime repellat debitis deserunt ut eum quidem
          sint, possimus nemo ad porro quae eligendi corrupti! Consectetur eaque
          nobis animi.
        </Typography>
        <Button variant='contained' color='secondary'>
          Learn More
        </Button>
      </Paper>
    </Container>
  );
};

export default PaperComponent;
