import React, { useState } from 'react';
import {
  Container,
  Paper,
  Box,
  Radio,
  RadioGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    backgroundColor: theme.palette.grey[300],
  },
}));

const RadioButtonComponent = () => {
  const classes = useStyles();
  const [gender, setGender] = useState('');

  const handleChange = (e) => {
    setGender(e.target.value);
  };

  return (
    <Container className={classes.root}>
      <Paper component={Box} width='30%' p={4} mx='auto'>
        <Box component='form'>
          <FormControl>
            <FormLabel>Choose your gender</FormLabel>
            <RadioGroup value={gender} onChange={handleChange} row>
              <FormControlLabel label='Male' control={<Radio />} value='male' />
              <FormControlLabel
                label='Female'
                control={<Radio />}
                value='female'
              />
            </RadioGroup>
          </FormControl>
        </Box>
      </Paper>
    </Container>
  );
};

export default RadioButtonComponent;
