import React from 'react';
import {
  Container,
  Paper,
  Typography,
  Button,
  Box,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    backgroundColor: theme.palette.grey[500],
    paddingTop: theme.spacing(5),
  },
}));

const TextFieldComponent = () => {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Paper component={Box} width='30%' mx='auto' p={4}>
        <Typography variant='h5'>Create new account</Typography>
        <Box component='form' mt={2}>
          <TextField
            fullWidth
            placeholder='Enter your First Name'
            margin='normal'
            variant='filled'
            color='secondary'
            label='First Name'
            helperText='enter your first name'
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <AccessAlarmIcon color='secondary' />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            fullWidth
            placeholder='Enter your Last Name'
            margin='normal'
            variant='outlined'
            label='Last Name'
          />
          <TextField
            fullWidth
            placeholder='Enter your Message'
            margin='normal'
            variant='outlined'
            label='Your Message'
            multiline
            rows='4'
            helperText='minimum 144 characters'
          />

          <Box>
            <Button type='submit' variant='outlined' color='primary'>
              Sign Up
            </Button>
          </Box>
        </Box>
      </Paper>
    </Container>
  );
};

export default TextFieldComponent;
