import React from 'react';
import { Container, Typography } from '@material-ui/core';
import deepPurple from '@material-ui/core/colors/deepPurple';
import teal from '@material-ui/core/colors/teal';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  mySubtitle: {
    color: teal[500],
  },
}));

const Colors = () => {
  const classes = useStyles();
  return (
    <Container>
      <Typography variant='h3' style={{ color: deepPurple['A700'] }}>
        Hello material ui
      </Typography>
      <Typography variant='subtitle2' className={classes.mySubtitle}>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Praesentium
        incidunt illum nihil velit? Asperiores provident eveniet repudiandae
        necessitatibus totam dolor, tempora ex veniam suscipit reprehenderit
        dolorum in labore impedit vel!
      </Typography>
    </Container>
  );
};

export default Colors;
