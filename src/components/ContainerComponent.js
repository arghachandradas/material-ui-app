import React from 'react';
import { Box, Container, Typography } from '@material-ui/core';

const ContainerComponent = () => {
  return (
    <Container maxWidth='sm'>
      <Box pt={4} textAlign='center'>
        <Typography variant='h4'>Why choose us ?</Typography>
        <Typography variant='subtitle1'>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Illum
          impedit, deleniti magni nemo odio aut porro, quasi maiores cum
          delectus deserunt. Nisi reprehenderit ut expedita aliquam totam nihil,
          illum accusamus, commodi iusto magnam non nostrum ab eius nobis.
          Cupiditate, distinctio?
        </Typography>
      </Box>
    </Container>
  );
};

export default ContainerComponent;
