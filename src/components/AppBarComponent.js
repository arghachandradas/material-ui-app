import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
  AppBar,
  Typography,
  Toolbar,
  Button,
  IconButton,
  Menu,
  MenuItem,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// import { lightGreen } from '@material-ui/core/colors';
import { MenuOpen } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

const AppBarComponent = () => {
  const classes = useStyles();
  const [mobileMenuAnchorEl, setMobileMenuAnchorEl] = useState(null);
  const isMobileMenuOpen = Boolean(mobileMenuAnchorEl);

  const openMobileMenu = (e) => {
    setMobileMenuAnchorEl(e.currentTarget);
  };
  const closeMobileMenu = () => {
    setMobileMenuAnchorEl(null);
  };

  const mobileMenu = (
    <Menu
      anchorEl={mobileMenuAnchorEl}
      id='mobile-menu'
      keepMounted
      open={isMobileMenuOpen}
      onClose={closeMobileMenu}
    >
      <MenuItem component={Link} onClick={closeMobileMenu} to='/'>
        Home
      </MenuItem>
      <MenuItem component={Link} onClick={closeMobileMenu} to='/about'>
        About
      </MenuItem>
      <MenuItem component={Link} onClick={closeMobileMenu} to='contact'>
        Contact
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <AppBar color='secondary'>
        <Toolbar>
          <Typography variant='h6' style={{ flexGrow: 1 }}>
            Material UI
          </Typography>
          <div className={classes.sectionDesktop}>
            <Button color='inherit' component={Link} to='/'>
              Home
            </Button>
            <Button color='inherit' component={Link} to='/about'>
              About
            </Button>
            <Button color='inherit' component={Link} to='/contact'>
              Contact
            </Button>
          </div>
          <IconButton
            color='inherit'
            onClick={openMobileMenu}
            className={classes.sectionMobile}
          >
            <MenuOpen />
          </IconButton>
        </Toolbar>
      </AppBar>
      {mobileMenu}
    </>
  );
};

export default AppBarComponent;
