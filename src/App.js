import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
// import ButtonComponent from './components/ButtonComponent';
// import Customization from './components/Customization';
// import TextFieldComponent from './components/TextFieldComponent';
// import RadioButtonComponent from './components/RadioButtonComponent';
// import CheckboxComponent from './components/CheckboxComponent';
// import ToggleSwitchComponent from './components/ToggleSwitchComponent';
// import ListComponent from './components/ListComponent';
// import TableComponent from './components/TableComponent';
import AppBarComponent from './components/AppBarComponent';
import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import { Box, Container } from '@material-ui/core';
// import Colors from './components/Colors';
// import ContainerComponent from './components/ContainerComponent';
// import PaperComponent from './components/PaperComponent';
// import BoxComponent from './components/BoxComponent';
// import TypographyComponent from './components/TypographyComponent';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <AppBarComponent />
      <Container component={Box} pt={4}>
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' exact component={About} />
          <Route path='/contact' exact component={Contact} />
        </Switch>
      </Container>
    </ThemeProvider>
  );
}

export default App;
